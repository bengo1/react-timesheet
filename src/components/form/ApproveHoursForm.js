"use strict";

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class FormApproveHours extends React.Component {
    constructor(props) {
        super(props);
        this.mixins = [PureRenderMixin];
    }
    render () {
        return <div className="form-approve-hours">
        <form>
            <div class="row">
                <div className="col-md-5 note-container">
                    <textarea placeholder="Notes"></textarea>
                </div>
                <div className="col-md-5 total-hours-container">
                    <div>
	                    <i className="clock-green icon"></i><br/>
                        <span className="total-label">Total Week Hours</span>
                    </div>
                    <div className="">
                        <span className="total-hours highlight">{this.props.totalHours}h</span>
                    </div>
                </div>
                <div className="col-md-2 submit-hours-button-container approval">
	                <button className="btn btn-lg btn-primary" ><i className="icon icon_approved_2"></i>Approve hours</button>
	                <button className="btn btn-lg btn-primary" ><i className="icon icon_reject_2"></i>Reject hours</button>
	                <a href="#/approved" >Approve Hours demo</a>
	                <br/>
	                <a href="#/rejecting" >Reject Hours demo</a>
	                <br/>
	                <a href="#/submit" >Submit Hours demo</a>
                </div>
            </div>
        </form>
        </div>;
    }
}
