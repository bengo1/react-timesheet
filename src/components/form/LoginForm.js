'use strict';

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.mixins = [PureRenderMixin];
        this.state = {};
    }

    validateField (event) {
	    event.preventDefault();
        var email = React.findDOMNode(this.refs.email).value,
            password = React.findDOMNode(this.refs.password).value,
            stateError = {"error" : {}};
        if (!email) {
            stateError.error.email = true;
        }
        if (!password) {
            stateError.error.password = true;
        }
        this.setState(stateError);
	    return false;
    }

    render() {
        var errorEmailClass = this.state.error && this.state.error.email ? "error on" : "error",
            errorPasswordClass = this.state.error && this.state.error.password ? "error on" : "error";

        return <div className="container">
                <div className="center-wrapper sm">
                    <h4>Welcome to <span className="highlight">Love People Timesheet system</span>, sign in to continue</h4>
                        <form action="/action/login">
                            <div className="form-group">
                                <span className={errorEmailClass}>Enter a valid email</span>
                                <input type="email" className="form-control" id="exampleInputEmail1" ref="email" placeholder="Enter email"/>
                                </div>
                            <div className="form-group">
                                <span className={errorPasswordClass}>Enter a valid password</span>
                                <input type="password" className="form-control" id="exampleInputPassword1"  ref="password" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <a href="#/clients">Sign in demo Link</a>
	                            <button type="submit" onClick={this.validateField.bind(this)} className="btn btn-default btn-lg right">
		                            Sign in<i className="icon icon_arrow_diagonal"></i>
	                            </button>
                            </div>
                        </form>
                </div>
        </div>;
    }
}

