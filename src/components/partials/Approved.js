'use strict';

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class Approved extends React.Component {
	constructor(props) {
		super(props);
		this.mixins = [PureRenderMixin];
	}

	render() {
		return (<div className="center-wrapper sm">
			<div className="info-grouped">
				<h4 className="highlight">Timesheet Approved</h4>
				<h4 className="highlight">[Freelancer Name] Week ending [week endind]</h4>
			</div>
			<div className="form-group">
				<a href="/#/approve" className="btn btn-default btn-lg"><i className="icon icon_arrow_backward"></i>Back to approval</a>
			</div>
		</div>);
	}
}

