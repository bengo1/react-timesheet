'use strict';

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class Footer extends React.Component {
	constructor(props) {
		super(props);
		this.mixins = [PureRenderMixin];
	}
    render () {
        return <footer className="footer">
	        <div className="container">
		        <div>
			        <img src={require('../../assets/images/lp_logo_grey_footer.png')} />
		        </div>
		        <div className="col-md-6">
			        <ul>
				        <li>Level 13, Studio 2</li>
				        <li>222 Pitt Street</li>
				        <li>Sydney NSW 2000</li>
				        <li>Australia</li>
				        <li>&nbsp;</li>
				        <li>+61 555 555 555</li>
				        <li>&nbsp;</li>
				        <li><a className="highlight" href="javascript:alert('not defined');">hello@lovepeople.com.au</a></li>
			        </ul>
		        </div>
		        <div className="col-md-6">
			        <ul>
				        <li>&copy; Love agency 2015. Made with <img src={require('../../assets/images/footer_heart_grey.png')} /> in Sydney, Australia</li>
			        </ul>
		        </div>
	        </div>
        </footer>;
    }
}
