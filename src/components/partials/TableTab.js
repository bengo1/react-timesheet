"use strict";

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class TableTab extends React.Component {
    constructor(props) {
        super(props);
        this.mixins = [PureRenderMixin];
	    this.state = {};
    }

	updateTab (activeButton) {
		this.setState({"active" : activeButton});
	}

    render () {
        var listItems = this.props.options.map(function(item) {
	        var className = "icon " + item.icon,
		        active = (!this.state.active && item.active) || this.state.active === item.label ? " on" : "",
				handler = function () { this.updateTab(item.label); item.action();}.bind(this),
		        buttonClass = "btn btn-lg btn-primary" + active;

            return <li className={buttonClass} onClick={handler}>
	            <i className={className}></i>
	            <i className="icon separator"></i>
                {item.label}
            </li>;
        }.bind(this));
        return <ul className="nav nav-tabs">{listItems}</ul>;
    }

}
