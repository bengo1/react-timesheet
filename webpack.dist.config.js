/*
 * Webpack distribution configuration
 *
 * This file is set up for serving the distribution version. It will be compiled to dist/ by default
 */

'use strict';

var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require("path");

module.exports = {

	output: {
		publicPath: '/assets/',
		path: 'dist/assets/',
		filename: 'main.js',
		cache: false
	},

	debug: true,
	devtool: "#inline-source-map",

	entry: [
		'webpack-dev-server/client?http://localhost:8001',
		'./src/app/main.js'],

	stats: {
		colors: true,
		reasons: false
	},

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.optimize.DedupePlugin(),
//  new webpack.optimize.UglifyJsPlugin(), // TODO check config for proper dev env
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.AggressiveMergingPlugin(),
		new ExtractTextPlugin("style.css", {
			allChunks: true
		})
	],

	resolve: {
		extensions: ['', '.js'],
		alias: {
			'styles': __dirname + '/src/styles',
			'mixins': __dirname + '/src/mixins',
			'components': __dirname + '/src/components/',
			'stores': __dirname + '/src/stores/',
			'actions': __dirname + '/src/actions/'
		}
	},

	module: {
		preLoaders: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader!jsxhint'
		}],
		loaders: [
		{
			test: /\.css$/,
			loader: ExtractTextPlugin.extract("css-loader")
		},
		{
			test: /\.scss$/,
			loader: ExtractTextPlugin.extract("css!sass?outputStyle=expanded&" +
				"includePaths[]=" +
				(path.resolve(__dirname, "./node_modules/bootstrap-sass/assets/stylesheets/"))
			)
		},
		{
			test: /\.(png|jpg)$/,
			loader: 'url-loader'
		},
		{
			test: /\.svg$|\.woff$|\.woff2$|\.ttf$|\.eot$|\.json$/,
			loader: "file"
		}]
	}
};
