'use strict';

var React = require('react/addons');
module.exports =  React.createClass({
    render: function () {
        var displayTotal = !!(this.props.addable),
            content = displayTotal ? React.DOM.tr({}, // display footer only if there are rules required
                this.props.cols.map(function (col, index) {
                        var total = '',
                            label = "Total",
                            colKey = col.key,
                            addable = this.props.addable && this.props.addable.indexOf(colKey) > -1;
                        if (addable) {
                            total = 0;
                            this.props.rows.forEach(function (row) {
                                total += +row[colKey];
                            });
                        }
                        total = (displayTotal && index === 0) ? label : total;
                        return React.DOM.td({}, <span>{total}</span>);
                    }.bind(this)
                )
            ) : "",
            tfoot = React.DOM.tfoot({}, content);
        return tfoot;
    }
});

// TODO make it BETTER!!