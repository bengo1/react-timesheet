'use strict';


describe('SorTableData', function () {
	var sorTableData;
	beforeEach(function () {

		sorTableData = require('components/SorTable/SorTableData.js');
	});

	describe('Data validation', function () {
		describe('validateHours', function () {

			it('should return false', function () {
				var input;
				expect(sorTableData.validate("Hours", input)).toBeFalsy();

				input = "";
				expect(sorTableData.validate("Hours", input)).toBeFalsy();

				input = null;
				expect(sorTableData.validate("Hours", input)).toBeFalsy();

				input = 25;
				expect(sorTableData.validate("Hours", input)).toBeFalsy();

				input = "8'";
				expect(sorTableData.validate("Hours", input)).toBeFalsy();

				input = "8/";
				expect(sorTableData.validate("Hours", input)).toBeFalsy();
			});
			it('should return true', function () {
				var input = "0";
				expect(sorTableData.validate("Hours", input)).toBeTruthy();

				input = "10";
				expect(sorTableData.validate("Hours", input)).toBeTruthy();

				input = 20;
				expect(sorTableData.validate("Hours", input)).toBeTruthy();

				input = 23.5;
				expect(sorTableData.validate("Hours", input)).toBeTruthy();
			});
		});

		describe('sortRows', function () {
			it('should sort values asc', function () {
				var order = 'asc',
					keyToSort = 'key';

				var data = [{'key': 1}, {'key': 3}, {'key': 2}],
					result = sorTableData.sortRows(data, keyToSort, order);
				expect(result[1].key).toEqual(2);

				data = [{'key': 'Albert'}, {'key': 'Roberto'}, {'key': 'Daniel'}];
				result = sorTableData.sortRows(data, keyToSort, order);
				expect(result[1].key).toEqual('Daniel');

				data = [{'key': '31/01/2001'}, {'key': '11/01/2001'}, {'key': '21/01/2001'}];
				var type = 'date';
				result = sorTableData.sortRows(data, keyToSort, order, type);
				expect(result[1].key).toEqual('21/01/2001');

				data = [{'key': '11'}, {'key': '333'}, {'key': '2'}];
				type = 'numeric';
				result = sorTableData.sortRows(data, keyToSort, order, type);
				expect(result[1].key).toEqual('11');
			});

			it('should sort values desc', function () {
				var order = 'desc',
					keyToSort = 'key';

				var data = [{'key': 1}, {'key': 3}, {'key': 2}],
					result = sorTableData.sortRows(data, keyToSort, order);
				expect(result[0].key).toEqual(3);

				data = [{'key': 'Albert'}, {'key': 'Roberto'}, {'key': 'Daniel'}];
				result = sorTableData.sortRows(data, keyToSort, order);
				expect(result[0].key).toEqual('Roberto');

				data = [{'key': '31/01/2001'}, {'key': '11/01/2001'}, {'key': '21/01/2001'}];
				var type = 'date';
				result = sorTableData.sortRows(data, keyToSort, order, type);
				expect(result[0].key).toEqual('31/01/2001');

				data = [{'key': '11'}, {'key': '333'}, {'key': '2'}];
				type = 'numeric';
				result = sorTableData.sortRows(data, keyToSort, order, type);
				expect(result[0].key).toEqual('333');
			});
		});
	});
});
