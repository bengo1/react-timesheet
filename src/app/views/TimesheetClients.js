"use strict";

var React = require('react/addons');
var SorTableRunner = require("../../components/SorTable/SorTableRunner");
var Footer = require("../../components/partials/Footer");
var Header = require("../../components/partials/Header");

export default class TimesheetClients extends React.Component {
    constructor (props) {
        super(props);
        this.config = {
            url: require('../../data/clients.json'),
            itemPerPage: 3,
            // key column features
            sortable: ["status", "employee", "week_ending", "total_hours"],
            addable: ["Hours"],
            validator: ["Hours"]
        };
        this.state = {};
        this.state.totalHours = 0;
    }
    render () {
        return (<div className="body-wrapper">
	        <div className="content-wrapper">
		        <Header />
		        <div className="center-wrapper container">
			        <SorTableRunner config={this.config}/>
		        </div>
	        </div>
	        <Footer />
        </div>);
    }
// TODO* Define local validators + business rules
// TODO externalise config to .json files
}

