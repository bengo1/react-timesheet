'use strict';

var React = require('react/addons');
var TestUtils = React.addons.TestUtils;

describe('Pagination', function () {
	var Pagination, component, componentInstance, container;

	beforeEach(function () {
		container = document.createElement('div');
		container.id = 'container';
		document.body.appendChild(container);

		Pagination = require('components/partials/Pagination.js');
		component = React.createElement(Pagination, {
			page: 1,
			update: jasmine.createSpy('spy'),
			itemPerPage: 3,
			itemsLength: 10
		});

		componentInstance = (function renderIntoDocument(instance) {
			return React.render(instance, container);
		}(component));
	});

	describe('Init', function () {
		it('should initialize a new instance of Pagination', function () {
			expect(component).toBeDefined();
			expect(componentInstance.props.page).toEqual(1);

			var disabledButtons = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "off");
			var buttons = TestUtils.scryRenderedDOMComponentsWithTag(componentInstance, "button");
			expect(disabledButtons.length).toEqual(1);
			expect(buttons.length).toEqual(2);

			var clickablePages = TestUtils.scryRenderedDOMComponentsWithTag(componentInstance, "li");
			expect(clickablePages.length).toEqual(4);
		});
	});

	describe('Events', function () {
		it('should trigger the callback passing the right arguments', function () {

			var disabledButtons = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "off");
			expect(disabledButtons.length).toEqual(1);

			TestUtils.Simulate.click(componentInstance.refs.next);
			expect(componentInstance.props.update).toHaveBeenCalledWith(2);
			expect(componentInstance.props.page).toEqual(2);

			TestUtils.Simulate.click(componentInstance.refs.next);
			expect(componentInstance.props.update).toHaveBeenCalledWith(3);
			expect(componentInstance.props.page).toEqual(3);

			TestUtils.Simulate.click(componentInstance.refs.prev);
			TestUtils.Simulate.click(componentInstance.refs.prev);
			expect(componentInstance.props.update).toHaveBeenCalledWith(1);
			expect(componentInstance.props.page).toEqual(1);
		});
	});
});
