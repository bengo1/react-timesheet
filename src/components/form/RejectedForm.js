'use strict';

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class RejectedForm extends React.Component {
	constructor(props) {
		super(props);
		this.mixins = [PureRenderMixin];
		this.state = {};
	}

	validateField (event) {
		event.preventDefault();
		var notes = React.findDOMNode(this.refs.notes).value,
			stateError = {"error" : {}};
		if (!notes) {
			stateError.error.notes = true;
		}
		this.setState(stateError);
		return false;
	}

	render() {
		var errorNotesClass = this.state.error && this.state.error.notes ? "error on" : "error";

			return <div className="center-wrapper sm">
				<form>
				<div className="info-grouped">
					<h4 className="highlight">Rejecting Timesheet</h4>
					<h4 className="highlight">[Freelancer Name] - Week ending [week endind]</h4>
				</div>
				<div className="info-grouped rejecting">
					<span className={errorNotesClass}>This field is empty</span>
					<textarea placeholder="Notes" ref="notes"></textarea>
				</div>
				<div className="form-group">
					<a href="/#/rejected" onClick={this.validateField.bind(this)} className="btn btn-default btn-lg right">Submit<i className="icon icon_arrow_diagonal"></i></a>
					<a href="/#/approve" className="btn btn-default btn-lg left"><i className="icon icon_arrow_backward"></i>Back</a>
				</div>
				</form>
			</div>;
	}
}

