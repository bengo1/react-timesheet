'use strict';

var React = require('react/addons');
var ReactTransitionGroup = React.addons.TransitionGroup;
var $ = require('jquery');


// JS
var SorTable = require('./SorTable');
var Pagination = require('../partials/Pagination');


// TODO Validating data passed to SorTable
var SorTableRunner = React.createClass({
    componentDidUpdate: function(nextProps) {
        if (nextProps.config.url !== this.props.config.url) {
            this.load();
        }
    },

    componentDidMount: function () {
        this.load();
    },

    load: function () {
        $.get(this.props.config.url, function(data) {
            if (this.isMounted()) {
                this.setState({
                    data: data,
                    page: 1
                });

                if (this.props.onLoad) {
                    this.props.onLoad(data.rows);
                }
            }
        }.bind(this));
    },

    updatePage: function (page) {
        this.setState({page: page});
    },

    render: function () {
        var data,
            page;
        if (!this.state || !this.state.data) {
            return (
                <div>
                    Loading Data...
                </div>
            );
        }
        else {
            data = this.state.data;
            page = this.state.page;
        }
        return (
            <div>
                <SorTable
                    data={data}
                    itemPerPage={this.props.config.itemPerPage}
                    page={page}
                    sortable={this.props.config.sortable}
                    editable={this.props.config.editable}
                    validator={this.props.config.validator}
                    onEdit={this.props.onEdit}
                    onLoad={this.props.onLoad}
                    addable={this.props.config.addable}
                    loaded={this.updateLength}
                />
                <Pagination page={page} update={this.updatePage} itemPerPage={this.props.config.itemPerPage} itemsLength={data.rows.length}/>
            </div>);
}
});
// TODO fallback when data not ready
module.exports = SorTableRunner;
