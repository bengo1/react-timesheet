'use strict';

var React = require('react/addons');

var SorTableHeader = require('./SorTableHeader');
var SorTableFooter = require('./SorTableFooter');
var SorTableRow = require('./SorTableRow');
var SorTableData = require('./SorTableData');


var SorTable = React.createClass({
    componentWillReceiveProps: function(nextProps) {
        if (nextProps.data.rows !== this.props.data.rows) {
            this.setState({
                rows: nextProps.data.rows
            });
        }
    },
    sort: function (colKey, order, dataType) {
        this.setState({
            "rows": SorTableData.sortRows(this.props.data.rows, colKey, order, dataType)
        });
    },
    setData: function (id, key, value) {
        var rows = this.props.data.rows.map(function (row) {
            if (id === row.id && row[key]) {
                row[key] = value;
            }
            return row;
        });
        this.setState({"rows": rows});
        this.props.onEdit(rows);
    },
    render: function () {
        var _self = this,
            perPage = this.props.itemPerPage,
            start = this.props.page * perPage - perPage,
            editable = this.props.editable || [],
            end = (this.props.page > 1) ? (this.props.page * perPage) : perPage,
            rows = (this.state && this.state.rows) ? this.state.rows: this.props.data.rows,
            displayRows = (perPage < rows.length) ? rows.slice(start, end) : rows,
            componentRows = [];
            displayRows.map(function (row, index) {
                componentRows.push(<SorTableRow
                    row={row}
                    key={row.id}
                    id={row.id}
                    setData={_self.setData}
                    cols={_self.props.data.cols}
                    editable={editable}
                    validator={_self.props.validator}
                    rowIndex={index}
                />);
            });
        return <div className="sorTable">
                {React.DOM.table({
                        className: "table table-striped"
                    }, [
                    <SorTableHeader cols={_self.props.data.cols} sortable={_self.props.sortable} sort={_self.sort}/>,
                    <SorTableFooter  cols={_self.props.data.cols} addable={_self.props.addable} rows={displayRows} />,
                    <tbody>{componentRows}</tbody>
                ])}
            </div>;
    }
});

module.exports = SorTable;

