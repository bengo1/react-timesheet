'use strict';

var React = require('react/addons');
var sorTableData = require('./SorTableData');
var $ = require('jquery');

var SorTableRow = React.createClass({
    componentDidUpdate: function () {
        if (this.state && this.state.editing) {
            this.refs[this.state.editing].getDOMNode().focus();
            this.refs[this.state.editing].getDOMNode().select();
        }
    },
    handleKeyDown: function (event, cell) {
        var ENTER = 13;
        if(event.keyCode === ENTER) {
            this.applyCell(cell);
        }
    },
    editCell: function (cell) {
        var state = {};
        state.editing = cell;
        this.setState(state);
    },
    applyCell: function (cell) {
        var state = {},
            valid = true;
        state.editing = false;
        state.error = false;
        if (this.props.validator && this.props.validator.indexOf(cell) > -1) {
            valid = this.validateCell(cell);
            if (!valid) {
                state.error = cell;
            }
        }
        state[cell] = React.findDOMNode(this.refs[cell]).value;

        this.setState(state);
        if (valid) {
            this.props.setData(this.props.id, cell, state[cell]);
        }
    },
    validateCell: function (cell) {
        return sorTableData.validate(cell, React.findDOMNode(this.refs[cell]).value);
    },
    render: function () {
        var cols = this.props.cols.map(function (col, index) {
		    var editable = (this.props.editable.indexOf(col.key) > -1),
			    cellContent = [],
			    refSpan = 'span' + col.key,
                refCol = 'col' + col.key,
			    classes,
			    value = (this.state && this.state[col.key]) ? this.state[col.key] : this.props.row[col.key],
			    cx = React.addons.classSet,
                errorClass = cx({
				    'error': !!(this.state && this.state.error && this.state.error === col.key)
			    });
		    // TODO add multiple error, use [] state.error

            var cell = (col.type === "link") ?
            <a href={this.props.row.action}>{col.link_label}</a> :
            <span ref={refSpan} key={refSpan} tabIndex={editable ? 0 : ""} className={errorClass}
                onClick={editable ? function (e) { this.editCell(col.key);}.bind(this) : null}
                onFocus={editable ? function (e) { this.editCell(col.key);}.bind(this) : null}>{value}</span>;


		    // add input to the editable cell
		    if (editable) {
			    var editing = (this.state && this.state.editing === col.key);
			    classes = React.addons.classSet({
				    "editable" : true,
				    "editing" : !!(editing)
			    });
			    cellContent.push(<input type="text" onKeyDown={function (e) {this.handleKeyDown(e, col.key);}.bind(this)} ref={col.key} key={col.key} onBlur={function () {this.applyCell(col.key);}.bind(this)} defaultValue={value} />);
		    }
            cellContent.push(cell);

		    return <td className={classes} ref={refCol} key={refCol}>{cellContent}</td>;
	    }.bind(this));
        return <tr key={this.props.id}>{cols}</tr>;
    }
});

module.exports = SorTableRow;
