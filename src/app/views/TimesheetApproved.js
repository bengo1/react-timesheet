"use strict";

var React = require('react/addons');
var Approved = require("../../components/partials/Approved");
var Footer = require("../../components/partials/Footer");
var Header = require("../../components/partials/Header");

export default class TimesheetApproved extends React.Component {
    constructor (props) {
        super(props);
        this.state = {};
    }

	render () {
		return (<div className="body-wrapper">
			<div className="content-wrapper">
				<Header />
				<div className="center-wrapper container">
					<Approved/>
				</div>
			</div>
			<Footer />
		</div>);
	}
}
