'use strict';

var $ = require('jquery');

var SorTableData = (function (window) {
    var _validator = {
        isNumber: function (input) {
            return !isNaN(parseFloat(input)) && isFinite(input);
        },
        validateHours : function (input) {
            if (!this.isNumber(input)) {
                return false;
            }
            if (input > 24) {
                return false;
            }
            return true;
        },
        // order : asc | desc
        // type : numeric, date
        sortRows: function (rows, key, order, type) {
            return rows.sort(function (row, next) {
	            var currentVal = this.preSort(row[key], type),
		            nextVal = this.preSort(next[key], type);
                if (order === 'asc') {
	                return (currentVal > nextVal) ? 1 : -1;
                } else {
	                return (currentVal > nextVal) ? -1 : 1;
                }
            }.bind(this));
        },
        // type : numeric, date
        preSort: function (value, type) {
            if (type === "numeric") {
                return +value;
            }
            if (type === "date") {
                return value.split("/").reverse().join("");
            }
            return value;
        },
	    getName: function (name) {
		    var validatorName = name.toLowerCase().replace(/[-_](.)/g, function(match, group1) {
			    return group1.toUpperCase();
		    });
		    return "validate" + validatorName.charAt(0).toUpperCase() + validatorName.substr(1);
	    }
    };

	var sorTableData = {
		validate: function (name, value) {
			var validatorName = _validator.getName(name);
			if (!_validator[validatorName] && typeof _validator[validatorName] !== "function") {
				throw "sorTableData:validator " + validatorName + " not found.";
			}
			return _validator[validatorName](value);
		},
		sortRows: _validator.sortRows.bind(_validator)
	};

	return sorTableData;

}(window));

module.exports = SorTableData;
// TODO test sort
