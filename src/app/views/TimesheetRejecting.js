"use strict";

var React = require('react/addons');
var RejectedForm = require("../../components/form/RejectedForm");
var Footer = require("../../components/partials/Footer");
var Header = require("../../components/partials/Header");

export default class TimesheetRejecting extends React.Component {
    constructor (props) {
        super(props);
        this.state = {};
    }

	render () {
		return (<div className="body-wrapper">
			<div className="content-wrapper">
				<Header />
				<div className="center-wrapper container">
					<RejectedForm/>
				</div>
			</div>
			<Footer />
		</div>);
	}
}
