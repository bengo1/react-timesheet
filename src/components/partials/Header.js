'use strict';

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class Header extends React.Component {
	constructor(props) {
		super(props);
		this.mixins = [PureRenderMixin];
	}
    render () {
        return  <header>
	        <div className="container">
		        <a href="#/"><img alt="logo" src={require('../../assets/images/lp_logo_header.png')}/></a>
		        <div className="right-element">
			        Hello <span id="header-name">[name]</span>
			        <i className="icon line_geen_bright"></i>
			        <a className="logout-button" href="#/">Logout</a>
		        </div>
	        </div>
        </header>;
  }
}
