'use strict';
require('../styles/scss/_includes.scss');

var TimesheetSubmit = require('./views/TimesheetSubmit');
var TimesheetApprove = require('./views/TimesheetApprove');
var TimesheetClients = require('./views/TimesheetClients');
var TimesheetLogin = require('./views/TimesheetLogin');
var TimesheetRejecting = require('./views/TimesheetRejecting');
var TimesheetApproved = require('./views/TimesheetApproved');
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;

var Routes = (
  <Route>
	  <Route name="home" path="/" handler={TimesheetLogin} />
	  <Route name="submit" path="/submit" handler={TimesheetSubmit} />
      <Route name="approve" path="/approve" handler={TimesheetApprove} />
      <Route name="clients" path="/clients" handler={TimesheetClients} />
	  <Route name="login" path="/login" handler={TimesheetLogin} />
	  <Route name="rejecting" path="/rejecting" handler={TimesheetRejecting} />
	  <Route name="approved" path="/approved" handler={TimesheetApproved} />
  </Route>
);

Router.run(Routes, function (Handler) {
    React.render(<Handler />, document.getElementById("react-wrapper"));
});
// TODO check router