'use strict';

var path = require('path');

module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            'test/helpers/**/*.js',
            'test/spec/components/**/*Test.js',
            'test/spec/stores/**/*.js',
            'test/spec/actions/**/*.js'
        ],
        preprocessors: {
            'test/spec/components/**/*Test.js': ['webpack', "coverage"],
            'test/spec/stores/**/*.js': ['webpack'],
            'test/spec/actions/**/*.js': ['webpack']
        },
        coverageReporter: {
            type : 'html',
            dir : 'coverage/'
        },
        webpack: {
            cache: true,
            module: {
                loaders: [{
                    test: /\.gif/,
                    loader: 'url-loader?limit=10000&mimetype=image/gif'
                }, {
                    test: /\.jpg/,
                    loader: 'url-loader?limit=10000&mimetype=image/jpg'
                }, {
                    test: /\.png/,
                    loader: 'url-loader?limit=10000&mimetype=image/png'
                }, {
                    test: /\.js$/,
                    loader: 'babel-loader'
                }, {
                    test: /\.scss/,
                    loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded'
                }, {
                    test: /\.css$/,
                    loader: 'style-loader!css-loader'
                }, {
                    test: /\.json/,
                    loader: 'json-loader'
                }]
            },
            resolve: {
                alias: {
                    'styles': path.join(process.cwd(), './src/styles/'),
                    'components': path.join(process.cwd(), './src/components/'),
                    'stores': '../../../src/stores/',
                    'actions': '../../../src/actions/'
                }
            }
        },
        webpackServer: {
            stats: {
                colors: true
            }
        },
        exclude: [],
        port: 8080,
        logLevel: config.LOG_INFO,
        colors: true,
        autoWatch: true,
        //usePolling: true, TODO check if needed
        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS', 'Chrome'], // TOO SLOW, debugging purpose
        //browsers: ['PhantomJS'],
        reporters: ['progress', 'coverage'],
        captureTimeout: 60000,
        singleRun: false
    });
};
