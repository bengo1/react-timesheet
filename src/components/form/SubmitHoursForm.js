"use strict";

var React = require('react/addons');
var PureRenderMixin = require('react/addons').addons.PureRenderMixin;

export default class FormSubmitHours extends React.Component {
    constructor(props) {
        super(props);
        this.mixins = [PureRenderMixin];
    }

    render () {
        return <div className="form-submit-hours">
        <form>
            <div class="row">
                <div className="col-md-5 note-container">
                    <textarea placeholder="Notes"></textarea>
                </div>
                <div className="col-md-5 total-hours-container">
                    <div className="">
                        <span className="total-label">
	                        <i className="clock-green icon"></i><br/>Total Week Hours</span>
                    </div>
                    <div className="">
                        <span className="total-hours highlight">{this.props.totalHours}h</span>
                    </div>
                </div>
                <div className="col-md-2 submit-hours-button-container">
	                <button  className="btn btn-lg btn-primary"><i className="icon_submit icon"></i><br/>Submit hours</button>
                </div>
            </div>
        </form>
        </div>;
    }
}
