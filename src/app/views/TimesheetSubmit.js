"use strict";

var React = require('react/addons');
var SorTableRunner = require("../../components/SorTable/SorTableRunner");
var SubmitHoursForm = require("../../components/form/SubmitHoursForm");
var Footer = require("../../components/partials/Footer");
var Header = require("../../components/partials/Header");

export default class TimesheetSubmit extends React.Component {
    constructor (props) {
        super(props);
        this.config = {
            url: require('../../data/week.json'),
            itemPerPage: 5,
            // key column features
            sortable: ["age", "hours"],
            editable: ["hours"],
            addable: ["hours"],
            validator: ["hours"]
        };
        this.state = {};
        this.state.totalHours = 0;
    }
    updateHours (rows) { // TODO consider using reflux:stores
        var totalHours = 0;
        rows.map(function (row) {
            totalHours += +row.hours;
        });
        this.setState({"totalHours": totalHours});
    }
    render () {
        return (<div className="body-wrapper">
	        <div className="content-wrapper">
	            <Header />
		        <div className="center-wrapper container">
	                <SorTableRunner config={this.config} onLoad={this.updateHours.bind(this)} onEdit={this.updateHours.bind(this)}/>
	                <SubmitHoursForm totalHours={this.state.totalHours}/>
	            </div>
	        </div>
	        <Footer/>
        </div>);
    }
// TODO* Define local validators + business rules
// TODO externalise config to .json files
}

