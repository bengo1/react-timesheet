'use strict';

var React = require('react/addons');
var TestUtils = React.addons.TestUtils;

//TODO rewire dependency [sorTableData, ], once bug fixed on babel ^5.0.0

describe('SorTableRow', function () {
    var SorTableRow, component, componentInstance, container;

    beforeEach(function () {
        container = document.createElement('div');
        container.id = 'container';
        document.body.appendChild(container);

        SorTableRow = require('components/SorTable/SorTableRow.js');
        component = React.createElement(SorTableRow, {
            row: [{"Name" : "Ben", "Age": 32}],
            key : 1,
            id: 1,
            setData : function () {},
            cols: [{
                "key": "Name",
                "label":"Name"
            },{
                "key": "Age",
                "label":"Age"
            }],
            editable: ["Age"],
            validator: ["Age"]
        });

        componentInstance = (function renderIntoDocument(instance) {
	        return React.render(instance, document.createElement('table'));
        }(component));
    });

    it('should create a new instance of SorTableRow', function () {
        expect(component).toBeDefined();
    });

    describe('Structure', function () {
        it('render rows with the right structure', function () {
            var cols = TestUtils.scryRenderedDOMComponentsWithTag(componentInstance, 'td');
            expect(cols.length).toEqual(2);

            var row = TestUtils.scryRenderedDOMComponentsWithTag(componentInstance, 'tr');
            expect(row.length).toEqual(1);

            var span = TestUtils.scryRenderedDOMComponentsWithTag(componentInstance, 'span');
            expect(span.length).toEqual(2);

            var input = TestUtils.scryRenderedDOMComponentsWithTag(componentInstance, 'input');
            expect(input.length).toEqual(1);

	        var cellEditable = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "editable");
	        expect(cellEditable.length).toEqual(1);
        });
    });


	describe('Events', function () {
		it('handlers and arguments are correct', function () {
			var spyEditCell = spyOn(componentInstance, 'editCell').and.callThrough();
            spyOn(componentInstance, 'validateCell').and.returnValue(true);

            TestUtils.Simulate.click(componentInstance.refs.spanAge);
			expect(spyEditCell).toHaveBeenCalledWith('Age');

            TestUtils.Simulate.focus(componentInstance.refs.spanAge);
            expect(spyEditCell.calls.count()).toEqual(2);

            var editingCell = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "editing");
            expect(editingCell.length).toEqual(1);

            var spyApplyCell = spyOn(componentInstance, 'applyCell').and.callThrough();

            React.addons.TestUtils.Simulate.keyDown(componentInstance.refs.Age, {keyCode: 13});
            expect(spyApplyCell).toHaveBeenCalledWith('Age');

            TestUtils.Simulate.blur(componentInstance.refs.Age);
            expect(spyApplyCell.calls.count()).toEqual(2);

            editingCell = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "editing");
            expect(editingCell.length).toEqual(0);
		});
	});

    describe('Validations', function () {
        it('error are displayed', function () {
            spyOn(componentInstance, 'validateCell').and.returnValue(false);

            componentInstance.applyCell('Age');
            expect(componentInstance.state.error).toBeDefined();
            expect(componentInstance.state.error).toEqual("Age");

            var errorCell = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "error");
            expect(errorCell.length).toEqual(1);
        });

        it('data are validated', function () {
            spyOn(componentInstance, 'validateCell').and.returnValue(true);

            componentInstance.applyCell('Age');
            expect(componentInstance.state.error).toEqual(false);

            var errorCell = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "error");
            expect(errorCell.length).toEqual(0);
        });
    });

});
