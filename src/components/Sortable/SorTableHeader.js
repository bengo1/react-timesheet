'use strict';

var React = require('react/addons');


var SorTableHeader =  React.createClass({
    componentDidUpdate: function (nextProps, nextState) {
        if (nextProps.sortable !== this.props.sortable) {
            this.setState({"sortable": this.updateSortableState()});
        }
    },
    getInitialState: function() {
        var state = {"sortable": this.updateSortableState()};
        return state;
    },
    updateSortableState: function (order, colKey) {
        var sortableKeys = {};
        // unsorted|asc|desc
        this.props.sortable.forEach(function (name) {
            // if order not passed, we initialise each sortable column
            if (!order) {
                sortableKeys[name] = "unsorted";
            } // otherwise we only set the column to update
            else {
                if(name === colKey) {
                    sortableKeys[name] = order;
                } else {
                    sortableKeys[name] = "unsorted";
                }
            }
        });
        return sortableKeys;
    },
    toggle: function (colKey) {
        var sort = (this.state.sortable[colKey] === "asc") ? "desc" : "asc",
            dataType = null;
        this.setState({"sortable": this.updateSortableState(sort, colKey)});
        // Passing the type is used to sort dat according to their type [date, numeric, ...]
        this.props.cols.forEach(function (col) {
            if (col.key === colKey && col.type) {
                dataType = col.type;
                return false;
            }
            return true;
        });
        this.props.sort(colKey, sort, dataType);
    },
    render: function () {
        var _self = this,
            thead = React.DOM.thead({},
                React.DOM.tr({},
                    this.props.cols.map(function (col) {

                        var colKey = col.key,
                            colLabel = col.label,
	                        sorTableAscArrow = _self.state.sortable[colKey] ? <span key={'arrowUp' + col.key} className="arrow-asc icon"></span> : "",
                            sorTableDescArrow = _self.state.sortable[colKey] ? <span key={'arrowDown' + col.key} className="arrow-desc icon"></span> : "",
                            sortClass = _self.state.sortable[colKey] ? 'sortable ' + _self.state.sortable[colKey] : '',
                            clickHandler = _self.state.sortable[colKey] ? function () {_self.toggle(colKey);} : null,
                            headerLabel = [];

                        headerLabel.push(colLabel);
                        headerLabel.push(sorTableAscArrow);
                        headerLabel.push(sorTableDescArrow);

                        return React.DOM.th({
                            ref: colKey,
	                        key: colKey,
                            className: sortClass,
                            onClick: clickHandler
                        }, headerLabel);

                    })));
        return thead;
    }
});

module.exports = SorTableHeader;
