# Description

Timesheet system built with React, Bootstrap SCSS, Jasmine - Karma, Grunt & Webpack

### Features

The main component is a sortable table configurable using json/ajax. 

Features implemented for now: sorting, editing, validating, ...

### Motivation

This project is a playground to use Webpack, React and ES6 (still in progress)


# Installation

Open the command line and type: 

`cd <project_root>` 

`npm install`

### Build process
Grunt build generates sprite, compile SCSS into minified CSS, minify and concat JS 

Grunt serve is running the app in a browser using webpack-dev-server 

`grunt build` 

`grunt serve`

### Tests
Run integration tests using Karma/PhantomJS 

`grunt test`