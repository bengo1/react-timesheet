'use strict';

var React = require('react/addons');


var Pagination = React.createClass({

    update: function (page) {
        this.props.update(page);
    },
    render: function () {
        var generikClass = "btn btn-default btn-lg ",
            numberOfPage = Math.ceil(this.props.itemsLength / this.props.itemPerPage),
            prevClass = generikClass + (this.props.page > 1 ? "": "off"),
            nextClass = generikClass + (numberOfPage !== +this.props.page  ? "": "off"),
            pageClass = numberOfPage > 1 ? "" : "off",
            pagesElmts = [],
            pages,
            onClickPage = function (page) {
                var pageClassName = (+i === +this.props.page) ? "current btn btn-default" : "";
                return <li onClick={function () {this.update(page);}.bind(this)} className={pageClassName}>{page}</li>;
            }.bind(this);
        for(var i = 1; i <= numberOfPage; i++) {
            pagesElmts.push(onClickPage(i));
        }

        pages = <ul className={pageClass}>{pagesElmts}</ul>;
        return <div className="row pagination">
            <div className="col-xs-4 col-md-4 prev">
                <button className={prevClass} ref="prev" onClick={function () {this.update(--this.props.page);}.bind(this)}><i className="icon icon_arrow_backward"></i>Previous page</button>
            </div>
            <div className="col-xs-4 col-md-4 pages">{pages}</div>
            <div className="col-xs-4 col-md-4 next">
                <button className={nextClass} ref="next" onClick={function () {this.update(++this.props.page);}.bind(this)}>Next page<i className="icon icon_arrow_forward"></i></button></div>
        </div>;
  }
});

module.exports = Pagination;
