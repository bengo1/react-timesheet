'use strict';

var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

var webpackDistConfig = require('./webpack.dist.config.js');

module.exports = function (grunt) {
    // Let *load-grunt-tasks* require everything
    require('load-grunt-tasks')(grunt);

    // Read configuration from package.json
    var pkgConfig = grunt.file.readJSON('package.json');

    grunt.initConfig({

        pkg: pkgConfig,

        webpack: {
            options: webpackDistConfig,
            dev: {
                cache: false
            }
        },

        'webpack-dev-server': {
            options: {
                hot: true,
                port: 8001,
                webpack: webpackDistConfig,
                publicPath: '/assets/',
                contentBase: './<%= pkg.src %>/'
            },

            start: {
                keepAlive: true
            }
        },

        connect: {
            options: {
                port: 8001
            },

            dev: {
                options: {
                    keepalive: true,
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, pkgConfig.dist)
                        ];
                    }
                }
            }
        },

        open: {
            options: {
                delay: 500
            },
            dev: {
                path: 'http://localhost:<%= connect.options.port %>/'
            }
        },

        karma: {
            unit: {
                configFile: 'karma.conf.js'
            }
        },
	    css_sprite: {
		    options: {
			    processor: 'scss',
			    retina: true,
			    margin: 0,
			    style: 'src/styles/scss/_icons.scss',
			    cssPath: '../../assets/sprites/',
			    template: 'src/assets/sprites/sprite.template.mustache'
		    },
		    sprite: {
			    src: ['src/assets/images/*.png'],
			    dest: 'src/assets/sprites/icons'
		    }
	    },
        copy: {
            dev: {
                files: [
                    // includes files within path
                    {
                        flatten: true,
                        expand: true,
                        src: ['<%= pkg.src %>/*'],
                        dest: '<%= pkg.dist %>/',
                        filter: 'isFile'
                    }
                ]
            }
        },

        clean: {
            dev: {
                files: [{
                    dot: true,
                    src: [
                        '<%= pkg.dist %>'
                    ]
                }]
            }
        }
    });
    grunt.loadNpmTasks('css-sprite');

    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build-light', 'open:dist', 'connect:dist']);
        }

        grunt.task.run([
            'open:dev',
            'webpack-dev-server'
        ]);
    });

    grunt.registerTask('build', function () {
        return grunt.task.run(['clean', 'css_sprite', 'copy', 'webpack']);
    });

    grunt.registerTask('test', ['karma']);

    grunt.registerTask('default', []);
};
