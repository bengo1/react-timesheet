"use strict";

var React = require('react/addons');
var SorTableRunner = require("../../components/SorTable/SorTableRunner");
var TableTab = require("../../components/partials/TableTab");
var ApproveHoursForm = require("../../components/form/ApproveHoursForm");
var Footer = require("../../components/partials/Footer");
var Header = require("../../components/partials/Header");

export default class TimesheetApprove extends React.Component {
    constructor (props) {
        super(props);
        this.state = {};
        this.state.config = {
            url: require('../../data/week.json'),
            itemPerPage: 3,
            // key column features
            sortable: ["age", "hours"],
            addable: ["Hours"]
        };
        this.tabOptions = [{
            label: "Pending timesheets (week loaded)",
            icon: "pending-timesheet",
            active: true,
            action: this.getPending.bind(this)
        }, {
            label: "Past timesheets  (clients)",
            icon: "approved-timesheet",
            action: this.getPast.bind(this)
        }
        ];
        this.state.totalHours = 0;
    }

    getPending () {
        this.setState({"config" : {
	        url: require('../../data/week.json'),
	        itemPerPage: 3,
	        // key column features
	        sortable: ["age", "hours"],
	        addable: ["Hours"]
        }});
    }

    getPast () {
        this.setState({"config" : {
	        url: require('../../data/clients.json'),
	        itemPerPage: 3,
	        // key column features
	        sortable: ["status", "employee", "week_ending", "hours"],
	        addable: ["hours"],
	        validator: ["hours"]
        }});
    }

    updateHours (rows) { // TODO consider using reflux:stores
        var totalHours = 0;
        rows.map(function (row) {
            totalHours += +row.hours;
        });
        this.setState({"totalHours": totalHours});
    }

    render () {
        return (<div className="body-wrapper">
            <div className="content-wrapper">
                <Header />
                <div className="center-wrapper container">
                    <TableTab options={this.tabOptions}/>
                    <SorTableRunner config={this.state.config} onLoad={this.updateHours.bind(this)}/>
                    <ApproveHoursForm totalHours={this.state.totalHours}/>
                </div>
            </div>
            <Footer />
        </div>);
    }

// TODO* Define local validators + business rules
// TODO externalise config to .json files
}
