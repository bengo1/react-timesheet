"use strict";

var React = require('react/addons');
var LoginForm = require("../../components/form/LoginForm");
var Footer = require("../../components/partials/Footer");
var Header = require("../../components/partials/Header");

export default class TimesheetLogin extends React.Component {
    constructor (props) {
        super(props);
        this.config = {
            url: require('../../data/week.json'),
            itemPerPage: 3,
            // key column features
            sortable: ["Age", "Hours"],
            onLoad: this.updateHours.bind(this),
            addable: ["Hours"],
            data: {
                rows: [],
                cols: []
            }
        };
        this.tabOptions = [{
            label: "Pending timesheets",
	        icon: "pending-timesheet",
            action: function(){console.log("Pending timesheets");}
        }, {
            label: "Past timesheets",
	        icon: "approved-timesheet",
	        action: function(){console.log("Past timesheets");}
        }];
        this.state = {};
        this.state.totalHours = 0;
    }

    updateHours (rows) { // TODO consider using reflux:stores
        var totalHours = 0;
        rows.map(function (row) {
            totalHours += +row.Hours;
        });
        this.setState({"totalHours": totalHours});
    }

	render () {
		return (<div className="body-wrapper">
			<div className="content-wrapper">
				<Header />
				<div className="center-wrapper container">
					<LoginForm/>
				</div>
			</div>
			<Footer />
		</div>);
	}

// TODO* Define local validators + business rules
// TODO externalise config to .json files
}
