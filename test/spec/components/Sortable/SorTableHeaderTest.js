'use strict';

var React = require('react/addons');
var TestUtils = React.addons.TestUtils;


describe('SorTableHeader', function () {
    var SorTableHeader, component, componentInstance, container;

    beforeEach(function () {
        container = document.createElement('div');
        container.id = 'container';
        document.body.appendChild(container);

        SorTableHeader = require('components/SorTable/SorTableHeader.js');
        component = React.createElement(SorTableHeader, {
            row: [{"Name" : "Ben", "Age": 32, "Id": 2}],
            setData : function () {},
            cols: [{
                "key": "Name",
                "label": "Name"
            },{
                "key": "Age",
                "label": "Age",
	            "type": "numeric"
            }, {
	            "key": "Id",
	            "label": "Id"
            }],
            sortable: ["Age", "Name"],
            sort: jasmine.createSpy('spy')
        });

        componentInstance = (function renderIntoDocument(instance) {
            return React.render(instance, document.createElement('table'));
        }(component));
    });

    it('should create a new instance of SorTableHeader', function () {
        expect(component).toBeDefined();
    });

	describe('Structure', function () {
		it('initialize table headers with the right structure', function () {
			var cols = TestUtils.scryRenderedDOMComponentsWithTag(componentInstance, 'th');
			expect(cols.length).toEqual(3);

			var colSortable = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "sortable");
			expect(colSortable.length).toEqual(2);

			var colNotSorted = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "unsorted");
			expect(colNotSorted.length).toEqual(2);
		});

		it('table headers have the right structure after interactions', function () {
			TestUtils.Simulate.click(componentInstance.refs.Age);

			var colNotSorted = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "unsorted");
			var colAscSorted = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "asc");
			expect(colNotSorted.length).toEqual(1);
			expect(colAscSorted.length).toEqual(1);

			TestUtils.Simulate.click(componentInstance.refs.Age);

			colAscSorted = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "asc");
			var colDescSorted = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "desc");
			colNotSorted = TestUtils.scryRenderedDOMComponentsWithClass(componentInstance, "unsorted");
			expect(colNotSorted.length).toEqual(1);
			expect(colAscSorted.length).toEqual(0);
			expect(colDescSorted.length).toEqual(1);
		});
	});

	describe('State', function () {
		it('component state is initialized correctly', function () {
			expect(componentInstance.state).toBeDefined();
			expect(componentInstance.state.sortable).toBeDefined();

			expect(componentInstance.state.sortable.Age).toBeDefined();
			expect(componentInstance.state.sortable.Age).toEqual('unsorted');

			expect(componentInstance.state.sortable.Name).toBeDefined();
			expect(componentInstance.state.sortable.Name).toEqual('unsorted');

		});

		it('each component state is correct', function () {
			TestUtils.Simulate.click(componentInstance.refs.Age);
			expect(componentInstance.state.sortable.Age).toEqual('asc');
			expect(componentInstance.state.sortable.Name).toEqual('unsorted');


			TestUtils.Simulate.click(componentInstance.refs.Name);
			expect(componentInstance.state.sortable.Name).toEqual('asc');
			expect(componentInstance.state.sortable.Age).toEqual('unsorted');

			TestUtils.Simulate.click(componentInstance.refs.Name);
			expect(componentInstance.state.sortable.Name).toEqual('desc');
			expect(componentInstance.state.sortable.Age).toEqual('unsorted');
		});
	});

    describe('Events', function () {
        it('handlers and arguments are correct', function () {
            var spyToggleCol = spyOn(componentInstance, 'toggle').and.callThrough();

            TestUtils.Simulate.click(componentInstance.refs.Age);

            expect(spyToggleCol).toHaveBeenCalledWith('Age');
	        expect(componentInstance.props.sort).toHaveBeenCalledWith('Age', 'asc', 'numeric');


	        TestUtils.Simulate.click(componentInstance.refs.Name);
	        expect(spyToggleCol.calls.count()).toEqual(2);
	        expect(componentInstance.props.sort).toHaveBeenCalledWith('Name', 'asc', null);

	        TestUtils.Simulate.click(componentInstance.refs.Name);
	        expect(spyToggleCol.calls.count()).toEqual(3);
	        expect(componentInstance.props.sort).toHaveBeenCalledWith('Name', 'desc', null);
        });
    });

});
